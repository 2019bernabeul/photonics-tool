import numpy as np
### ------ ONE-PHOTON MATRICES ------
### Pauli matrices

X = np.array([[0, 1], [1, 0]], dtype="complex")
Y = np.array([[0, 0.-1j], [0+1j, 0]], dtype = "complex")
Z = np.array([[1, 0], [0, -1]], dtype = "complex")

### Other One-qubit matrices

H = (1/np.sqrt(2))*np.array([[1, 1],
                             [1, -1]], dtype='complex')
H_REV = (1/np.sqrt(2))*np.array([[-1, 1],
                                 [1, 1]], dtype='complex')
S = np.array([[1, 0], [0, 0+1j]], dtype='complex')
T = np.array([[1, 0], [0, np.exp(complex(0, np.pi/8))]], dtype="complex")


QUARTER_WAVE_RIGHT_CIRCULAR_RETARDER = (1/np.sqrt(2))*np.array([[1j, 1j],
                                                                [-1j, 1j]])
QUARTER_WAVE_LEFT_CIRCULAR_RETARDER = QUARTER_WAVE_RIGHT_CIRCULAR_RETARDER.T

HALF_WAVE_RIGHT_CIRCULAR_RETARDER = 1j*Y
HALF_WAVE_LEFT_CIRCULAR_RETARDER = 1j*Y.T

### Matrix functions

def PHASE(angle):
    return np.array([[1, 0], [0, np.exp(complex(0, angle))]], dtype="complex")

def LINEAR_POLARIZER(theta):
    return np.array([[np.cos(theta)*np.cos(theta), np.cos(theta)*np.sin(theta)],
                     [np.cos(theta)*np.sin(theta), np.sin(theta)*np.sin(theta)]])

def ELLIPTICAL_POLARIZER(epsilon, psi):
    J11 = np.cos(psi)**2 + (epsilon*np.sin(psi))**2
    J12 = 1j*epsilon + (epsilon**2 - 1)*np.cons(psi)*np.sin(psi)
    J22 = (epsilon*np.cos(psi))**2 + np.sin(psi)**2
    return (1/(1+epsilon**2))*np.array([[J11, J12],
                                        [-J12, J22]])

def LINEAR_RETARDER(delta, theta): ### symmetric phase convention
    L11 = np.exp(-1j*delta/2)*(np.cos(theta)**2) + np.exp(1j*delta/2)*(np.sin(theta)**2)
    L12 =-1j*np.sin(delta/2)*np.sin(2*theta)
    L22 = np.exp(1j*delta/2)*(np.cos(theta)**2) + np.exp(-1j*delta/2)*(np.sin(theta)**2)
    return np.array([[L11, L12],
                     [L12, L22]])


### ------ TWO-PHOTON MATRICES ------

SYM_BS_AMPLITUDE_MATRIX = (1/np.sqrt(2))*np.array([[1j, 1],
                                                   [1, 1j]])

HALF_MIRROR_LEFT_PLATE_AMPLITUDE_MATRIX = H_REV
HALF_MIRROR_RIGHT_PLATE_AMPLITUDE_MATRIX = H

SYM_BS_INACTIVE = np.kron(SYM_BS_AMPLITUDE_MATRIX, np.eye(2))
HALF_MIRROR_LEFT_INACTIVE = np.kron(HALF_MIRROR_LEFT_PLATE_AMPLITUDE_MATRIX, np.eye(2))
HALF_MIRROR_RIGHT_INACTIVE = np.kron(HALF_MIRROR_RIGHT_PLATE_AMPLITUDE_MATRIX, np.eye(2))

SYM_BS_ACTIVE = (1/np.sqrt(2))*np.array([[-1j, 0,   1,   0],
                                           [0,  1j,   0,   1],
                                           [1,   0, -1j,   0],
                                           [0,   1,   0,  1j]], dtype='complex')

HALF_MIRROR_LEFT_ACTIVE = (1/np.sqrt(2))*np.array([[-1,  0,   1,   0],
                                                   [0,   1,   0,   1],
                                                   [1,   0,   1,   0],
                                                   [0,   1,   0,  -1]], dtype='complex')

HALF_MIRROR_RIGHT_ACTIVE = (1/np.sqrt(2))*np.array([[1,   0,   1,   0],
                                                    [0,  -1,   0,   1],
                                                    [1,   0,  -1,   0],
                                                    [0,   1,   0,   1]], dtype='complex')

POLARISER_BS = np.array([[1,   0,   0,   0],
                        [0,  0,   1,   0],
                        [0,   0,  0,   1],
                        [0,   1,   0,   0]], dtype='complex')
