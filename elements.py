import numpy as np

### ------ Special elements ------
class Source:
    def __init__(self, initial_vector=np.array([1., 0.+0j]), time = 0): ## if no input state given, starts a |0>
        self.photon = initial_vector
        self.next = None
        self.time = time

    def out(self, side):
        return self.photon
    
    def temporality(self, side):
        return self.time


class Screen:
    def __init__(self, previous=(None, None)):
        self.previous = previous

    def show(self, name = 'ψ'):
        previous_component, previous_component_output_side = self.previous
        ket = previous_component.out(previous_component_output_side)
        alpha = round(ket[0],3)
        beta = round(ket[1],3)
        print(f"|{name}⟩ = {alpha}|0⟩ + {beta}|1⟩")

    def out(self):
        previous_component, previous_component_output_side = self.previous
        return previous_component.out(previous_component_output_side)

    def temporality(self):
        previous_component, previous_component_output_side = self.previous
        return previous_component.temporality(previous_component_output_side) 



### ------ Classic Elements ------
class Element:
    def __init__(self, previous): ## previous is either a single (previous_object, output) tuple, or a 2-dictionary of tuples
        self.previous = previous  ## (as in {"left": (Object 1, "label of chosen output of object 1"), "right": (obj2, "label2")})
        self.next = None

class Path(Element):
    def __init__(self, previous, time):
        Element.__init__(self, previous)
        self.time = time

    def out(self, side):
        previous_component, previous_component_output_side = self.previous
        return previous_component.out(previous_component_output_side)
    
    def temporality(self, side):
        previous_component, previous_component_output_side = self.previous
        return previous_component.temporality(previous_component_output_side) + self.time

class Plate(Element):
    def __init__(self, previous, jones_matrix = np.eye(2, dtype='complex')):
        Element.__init__(self, previous)
        self.jones_matrix = jones_matrix
        self.output_vector = None

    def out(self, side):
        previous_element = self.previous[0]
        previous_element_output_side = self.previous[1]
        self.output_vector = self.jones_matrix @ previous_element.out(previous_element_output_side) # matrix-vector product : jones matrix times state vector given by the out(side) method of the previous component
        return self.output_vector
    
    def temporality(self, side):
        previous_component, previous_component_output_side = self.previous
        return previous_component.temporality(previous_component_output_side)

class BeamSplitter(Element):
    """matrix is a 4x4 complex ndarray, two first dimensions regard left input, two next are right. So it's blockwise defined."""
    def __init__(self, previous, matrix = np.eye(4, dtype='complex')):
        Element.__init__(self, previous)
        self.matrix = matrix
        self.output_vector = None

    def out(self, side):
        if self.previous["left"] is None:
            left_input_state = np.zeros((2,), dtype="complex") # if no input on the left, then |\psi> = 0
        else:
            left_input_element = self.previous["left"][0]
            left_input_element_output_side = self.previous["left"][1]
            left_input_state = left_input_element.out(left_input_element_output_side)

        if self.previous["right"] is None:
            right_input_state = np.zeros((2,), dtype="complex") # if no input on the right, then |\psi> = 0
        else:
            right_input_element = self.previous["right"][0]
            right_input_element_output_side = self.previous["right"][1]
            right_input_state = right_input_element.out(right_input_element_output_side)

        input_state = np.concatenate((left_input_state, right_input_state))
        self.output_vector = self.matrix @ input_state

        if side == "left":
            ans, _ = np.split(self.output_vector, 2)

        elif side == "right":
            _, ans = np.split(self.output_vector, 2)

        return ans

    def temporality(self, side):
        if self.previous["left"] is None and self.previous["right"] is None:
            return 0

        elif self.previous["left"] is None:
            right_previous_component = self.previous["right"][0]
            right_previous_component_output_side = self.previous["right"][1]
            right_time = right_previous_component.temporality(right_previous_component_output_side)
            return right_time

        elif self.previous["right"] is None:
            left_previous_component = self.previous["left"][0]
            left_previous_component_output_side = self.previous["left"][1]
            left_time = left_previous_component.temporality(left_previous_component_output_side)
            return left_time
        else:
            right_previous_component = self.previous["right"][0]
            right_previous_component_output_side = self.previous["right"][1]
            right_time = right_previous_component.temporality(right_previous_component_output_side)
            left_previous_component = self.previous["left"][0]
            left_previous_component_output_side = self.previous["left"][1]
            left_time = left_previous_component.temporality(left_previous_component_output_side)
            if side == "left":
                return left_time
            elif side == "right":
                return right_time