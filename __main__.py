import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import elements
import graphics
import matrices
from math import sqrt

print("Hi World!")

## Tests
'''print("###------ Portes anticommutantes ------")
source = elements.Source(initial_vector = np.array([(3/4)*(1. + 0j), (4/5)*(1. + 0j)]))

elements.Screen(previous = (source, "left")).show()

BS1 = elements.BeamSplitter(previous = {"left": None, "right": (source, "left")}, matrix=matrices.HALF_MIRROR_LEFT_ACTIVE)

elements.Screen(previous = (BS1, "left")).show("left")
elements.Screen(previous = (BS1, "right")).show("right")

X_left_1 = elements.Plate(previous = (BS1, "left"), jones_matrix=matrices.X)
Z_left_2 = elements.Plate(previous = (X_left_1, "left"), jones_matrix = matrices.Z)


Z_right_1 = elements.Plate(previous = (BS1, "right"), jones_matrix = matrices.Z)
X_right_2 = elements.Plate(previous = (Z_right_1, "left"), jones_matrix = matrices.X)

BS2 = elements.BeamSplitter(previous = {"left": (X_right_2, "left"), "right": (Z_left_2, "left")}, matrix = matrices.HALF_MIRROR_RIGHT_ACTIVE)

screen_right = elements.Screen(previous = (BS2, "right"))
screen_left = elements.Screen(previous = (BS2, "left"))

screen_left.show("left")
screen_right.show("right")'''

'''print("### ------ Portes commutantes ------")

source2 = elements.Source(initial_vector = np.array([1.+0j, 0.]))

BS12 = elements.BeamSplitter(previous = {"left": (source2, "left"), "right": None}, matrix=matrices.SYM_BS_INACTIVE)

elements.Screen(previous = (BS12, "left")).show()
elements.Screen(previous = (BS12, "right")).show()

Phase_left_12 = elements.Plate(previous = (BS12, "left"), jones_matrix=matrices.PHASE(np.pi/5))
Z_left_22 = elements.Plate(previous = (Phase_left_12, "left"), jones_matrix = matrices.Z)


Z_right_12 = elements.Plate(previous = (BS12, "right"), jones_matrix = matrices.Z)
Phase_right_22 = elements.Plate(previous = (Z_right_12, "left"), jones_matrix = matrices.PHASE(np.pi/5))

BS22 = elements.BeamSplitter(previous = {"left": (Z_left_22, "left"), "right": (Phase_right_22, "left")}, matrix = matrices.SYM_BS_INACTIVE)

screen_right2 = elements.Screen(previous = (BS22, "right"))
screen_left2 = elements.Screen(previous = (BS22, "left"))

screen_left2.show("left")
screen_right2.show("right")'''


'''print("###------ Tests BeamSplitters ------")
source = elements.Source(initial_vector = np.array([(3/5)*(1. + 0j), (4/5)*(1. + 0j)]))

elements.Screen(previous = (source, "left")).show()

BS1 = elements.BeamSplitter(previous = {"left": None, "right": (source, "left")}, matrix=matrices.HALF_MIRROR_LEFT_ACTIVE)

elements.Screen(previous = (BS1, "left")).show("left")
elements.Screen(previous = (BS1, "right")).show("right")'''

'''print("###------ Portes classiques ------")
source = elements.Source(initial_vector = np.array([1. + 0j, 0.]))

elements.Screen(previous = (source, "left")).show()

Nom =   {str(matrices.X) : "X",
        str(matrices.Y) : "Y",
        str(matrices.Z) : "Z",
        str(matrices.H) : "H",
        str(matrices.S) : "S",
        str(matrices.T) : "T",}

Portes =    [matrices.X,
            matrices.Y,
            matrices.Z,
            matrices.H,
            matrices.S,
            matrices.T]

for porte1 in Portes:
    P1 = elements.Plate(previous = (source, "left"), jones_matrix=porte1)
    elements.Screen(previous = (P1, "left")).show( Nom[str(porte1)] + "|ψ⟩")
    for porte2 in Portes:
        P2 = elements.Plate(previous = (P1, "left"), jones_matrix=porte2)
        elements.Screen(previous = (P2, "left")).show(Nom[str(porte2)] + Nom[str(porte1)] + "|ψ⟩")'''

print("###------ Tentative marche aléatoire ------")


def marche(n):
    Photons = [elements.Source(initial_vector = np.array([0., 1.+0j]))]
    for i in range(n):
        New_Photons = []
        new_photons = dict()
        for source in Photons:
            PH = elements.Plate(previous = (source, "left"), jones_matrix=matrices.H)
            BS = elements.BeamSplitter(previous = {"left": (PH, "left"), "right": None}, matrix=matrices.POLARISER_BS)
            LEFT = elements.Path(previous = (BS, "left"), time = -1)
            RIGHT = elements.Path(previous = (BS, "right"), time = 1)
            Screen_left = elements.Screen(previous = (LEFT, "left"))
            Screen_right = elements.Screen(previous = (RIGHT, "right"))
            if Screen_left.temporality() in new_photons.keys():
                new_photons[Screen_left.temporality()][0] += Screen_left.out()[0]
                new_photons[Screen_left.temporality()][1] += Screen_left.out()[1]
            else:
                new_photons[Screen_left.temporality()] = np.array([Screen_left.out()[0], Screen_left.out()[1]])
            if Screen_right.temporality() in new_photons.keys():
                new_photons[Screen_right.temporality()][0] += Screen_right.out()[0] 
                new_photons[Screen_right.temporality()][1] += Screen_right.out()[1]
            else:
                new_photons[Screen_right.temporality()] = np.array([Screen_right.out()[0], Screen_right.out()[1]])
        Photons = [elements.Source(initial_vector = list(new_photons.values())[k] ,time = list(new_photons.keys())[k] ) for k in range(len(new_photons))]
    repartition = dict()
    for photon in Photons:
        if photon.temporality('left') in repartition.keys():
            repartition[photon.temporality('left')] += photon.out('left')[0]**2 + photon.out('left')[1]**2
        else:
            repartition[photon.temporality('left')] = photon.out('left')[0]**2 + photon.out('left')[1]**2
    plt.plot(list(repartition.keys()),list(repartition.values()))
    plt.title("Distribution de densité de probabilité")
    plt.xlabel("Position")
    plt.ylabel("Probabilité de présence")
    plt.show()
marche(100)